#include "game.h"

using namespace Sifteo;

static Metadata M = Metadata()
    .title("ANTZ")
    .package("com.antz.game", "1.1")
	.icon(Icon)
    .cubeRange(0, CUBE_ALLOCATION);

void main(void) {
	Game game;
	game.init();	//Initialize a/the game.
	game.run();		//Blocking.
	
}
