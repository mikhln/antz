#include "ant.h"
#include "cube.h"

/*
	Start values are set for the ant.
	Like what walkingspeed, direction,
	where on the cube it starts
	and on which cube. 
	.
*/
Ant::Ant() {
	speed = 30;		
	x = 64.0 - 7.5;
	y = 64.0 - 7.5;
	direction = RIGHT;
	cubeID = 0;
	hasChangedDirection=false;
}

void Ant::setCubeRef(Cube* cube){
	this->currCube=cube;
}

void Ant::update(TimeDelta td){
	if(direction == TOP){
		y = y - this->speed * float(td);	//Move the ant towards the top.
	}
	else if(direction == RIGHT){
		x = x + this->speed * float(td);	//Move the ant towards the right.
	}
	else if(direction == BOTTOM){
		y = y + this->speed * float(td);	//Move the ant towards the botton.
	}
	else if(direction == LEFT){
		x = x - this->speed * float(td);	//Move the ant towards the left.
	}

	//When the ant is in the vicinity of the centre of the cube a new direction is randomized.
	if(x<(65-7.5) && x>(63-7.5) && y<(65-7.5) && y>(63-7.5)){
		this->newDirection();				
	}
}


	//Speed is increased by 1.
void Ant::incSpeed() {
	speed++;
}
	
Float2 Ant::getPos(){
	return vec(x,y);
}

void Ant::setCube(CubeID c,Side side,Cube* cube){
	this->setCubeRef(cube);
	x = 64.0 - 7.5;		//Centers the ant, one of the coordinates will change in the if-statements.
	y = 64.0 - 7.5;

	if(side==TOP){
		y=1;
		direction=BOTTOM;
	}

	if(side==RIGHT){
		x=127;
		direction=LEFT;
	}

	if(side==LEFT){
		x=1;
		direction=RIGHT;
	}

	if(side==BOTTOM){
		y=127;
		direction=TOP;
	}

	cubeID = c;
	this->incSpeed();
	this->hasChangedDirection = false;
}

int Ant::getCube(){
	return cubeID;
}

bool Ant::isOutside(){
	return (x<0||x>128||y<0||y>128);
}

Side Ant::outsideSide(){
	if(x<0){return LEFT;}
	if(x>128){return RIGHT;}
	if(y<0){return TOP;}
	if(y>128){return BOTTOM;}
	return TOP;		//A return that will never be used but is needed for the compiler to be happy.
}

/*
	Non-deterministic randomizer for a new walking direction.
*/
void Ant::newDirection(){
	if(!hasChangedDirection){
	Random r;
	int randSide=r.randint(0,3);

	//Can not take the new direction if there is a wall in that direction.
	if(randSide==0 && !this->currCube->wallTOP){
		direction=TOP;
		hasChangedDirection=true;
		return;
	}

	if(randSide==1 && !this->currCube->wallRIGHT){
		direction=RIGHT;
		hasChangedDirection=true;
		return;
	}

	if(randSide==2 && !this->currCube->wallLEFT){
		direction=LEFT;
		hasChangedDirection=true;
		return;
	}

	if(randSide==3 && !this->currCube->wallBOTTOM){
		direction=BOTTOM;
		hasChangedDirection=true;
		return;
	}

	this->newDirection();	//Another direction is randomized (hopefully not a wall in that direction).
	}

}