#include "game.h"

void Game::init() {
	Events::cubeTouch.set(&Game::onCubeTouch, this);
	this->newGame = false;
	Ant ant = Ant();
	this->ant = &ant;

	for(int i=0;i<NUMCUBES;i++){
		this->cube[i].init(i);	
	}

	this->ant->setCubeRef(&cube[0]);
	this->ant->newDirection();

	this->frame=0;
	this->rungame=true;

	this->gameCounter==0;
}

void Game::onCubeTouch(unsigned int id){
	//If a cube that the ant is not on is touched during a game
	// the cube will get a new set of walls.
	if(int(this->ant->getCube()) != id){
		this->cube[id].RandWalls();
	}

	//If a cube is touched when the game has ended
	//the system menu is shown.
	if(this->rungame==false){
		this->newGame=true;
	}
}

void Game::run() {

	//Game loop!
	while(rungame==true){
		ts.next();

		this->tick(ts.delta());
		System::paint();
	}

	/*
	The game has ended.
	To be able to write text the video mode is changed.
	*/
	for(int i=0;i<NUMCUBES;i++){
		this->cube[i].vid.initMode(BG0_ROM);	
		this->cube[i].vid.bg0rom.text(vec(2,2),"Game OVER");
		String<25> s;
		s << "You managed \n" << this->gameCounter << " screen(s).";
		this->cube[i].vid.bg0rom.text(vec(2,3),s);
	}

	//Untill the screen is pressed display the score.
	while(!this->newGame){
		System::paint();
	}
}

void Game::tick(TimeDelta td) {
	this->ant->update(td);	//Move the ant internally.
	Float2 pos=this->ant->getPos();	//Get the new position.

	//Gets the correct image for the ant depending on its walkingdirection.
	PinnedAssetImage pai;	
	if(this->ant->direction==TOP)pai=AntSprite;
	if(this->ant->direction==LEFT)pai=AntSpriteLeft;
	if(this->ant->direction==RIGHT)pai=AntSpriteRight;
	if(this->ant->direction==BOTTOM)pai=AntSpriteBottom;

	//Animates the ant by changing the frame in the sprite.
	this->cube[this->ant->getCube()].vid.sprites[0].setImage(pai, frame % AntSprite.numFrames());

	//Move the ant to the new position on the screen.
	this->cube[this->ant->getCube()].vid.sprites[0].move(pos);

	//If statement for when the ant walks out of the cube.
	if (this->ant->isOutside()){
		//If statement that checks if there was another cube where the ant exited.
		if (this->cube[this->ant->getCube()].vid.physicalNeighbors().hasCubeAt(this->ant->outsideSide())){
			this->cube[this->ant->getCube()].RandWalls();	//Randomize new set of walls on the cube that the ant left.
			CubeID entryCubeBeforeItsAint=this->cube[this->ant->getCube()].vid.physicalNeighbors().cubeAt(this->ant->outsideSide());
			int entryCube=int(entryCubeBeforeItsAint);
			Side entrySide=this->cube[entryCube].vid.physicalNeighbors().sideOf(this->ant->getCube());

			this->cube[this->ant->getCube()].vid.sprites[0].hide();
			
			//Check if the ant ran into a wall on the entrycube.
			if ((entrySide==TOP && this->cube[entryCube].wallTOP) ||
				(entrySide==LEFT && this->cube[entryCube].wallLEFT) ||
				(entrySide==RIGHT && this->cube[entryCube].wallRIGHT) ||
				(entrySide==BOTTOM && this->cube[entryCube].wallBOTTOM)){
					this->rungame=false;	//If you hit a wall you lost!
			}
			
			this->gameCounter++;	//Count how many cubes you have traversed.

			//The actual moving of the ant to the new cube.
			this->ant->setCube(cube[this->ant->getCube()].vid.physicalNeighbors().cubeAt(this->ant->outsideSide()),entrySide,&cube[entryCube]);
																															
		}
		
		//If there was no new cube where the ant exited you lost!
		else{
			this->rungame=false;
		}	
	}

	//Update the frame for the ant.
	frame = (frame == 2) ? 0 : frame+1; 

}