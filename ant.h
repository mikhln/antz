#include <sifteo.h>
#include "assets.gen.h"


using namespace Sifteo;
class Cube;

class Ant {
public:
	int direction; //Represents the direction that the ant is currently walking in.
	
	Side outsideSide(); //Returns the side that the ant exited from.
	Ant(); //Constructor.
	void update(TimeDelta td); //Updates what the ant is doing each timestep.
	Float2 getPos();  //Get the position of the upper left corner of the ant.
	void incSpeed();  //Increases the ants walkingspeed.
	int getCube();  //Returns id of the cube that the ant is on.
	void setCube(CubeID id,Side side,Cube* cube);  //Changes the cube that the ant is on.
	bool isOutside();   //Checks if the ant is outside the cube or not.
	void newDirection(); //Method that randomizes a new direction for the ant to walk.
	void setCubeRef(Cube* cube);  //Sets the cube that the ant is on (from the start).

private:

	float speed; //The walkingspeed of the ant.
	float x; // x coordinate
	float y; // y coordinate

	CubeID cubeID; // The id of the current cube.
	Cube* currCube; //Reference to the cube that the ant is on.
	bool hasChangedDirection;  //Makes sure that the ant can only change direction once per cube.
};