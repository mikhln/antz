-- Metadata

IconAssets = group{quality=9.95}
Icon = image{"Assets/icon.png"}

GameAssets = group{}
Background = image{"Assets/ant_bg2.png", quality=10}

--The ants
AntSprite = image{"Assets/ant.png", pinned=true, width=16, height=16}
AntSpriteLeft = image{"Assets/ant_left.png", pinned=true, width=16, height=16}
AntSpriteRight = image{"Assets/ant_right.png", pinned=true, width=16, height=16}
AntSpriteBottom = image{"Assets/ant_bottom.png", pinned=true, width=16, height=16}

--The walls
Horizontal = image{"Assets/horizontal.png" , pinned=true,width=128, height=16}
Vertical = image{"Assets/vertical.png" , pinned=true,width=16, height=128}