#include <sifteo.h>

class Game;

using namespace Sifteo;

class Cube
{
public:
	bool wallTOP;	//True if there is a wall on top of the cube false otherwise.
	bool wallLEFT;  //True if there is a wall to the left in thecube false otherwise.
	bool wallRIGHT;	//True if there is a wall to the right in the cube false otherwise.
	bool wallBOTTOM; //True if there is a wall in the bottom of the cube false otherwise.
	CubeID id;  //The id of the cube.
	VideoBuffer vid;  //The videobuffer for the cubes graphics.

	void init(CubeID cubeId);  //Initializes the cube.
	void RandWalls();	//Method that randomize walls to the cubes. 
};

