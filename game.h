#include <sifteo.h>
#include "assets.gen.h"
#include "cube.h"
#include "ant.h"
#define NUMCUBES 3

using namespace Sifteo;

class Game {
public:
	Cube cube[NUMCUBES];  //Array of cubes available in the game.
	Ant *ant;		//Reference to the ant that the game is about.
	TimeStep ts;	//A timestep.
	int frame;		
	bool rungame;  //True if the game is running, false otherwise.

	void init();	//Initializes the game.
	void run();		//Gameloop.
	void tick(TimeDelta td);	//Method that is executes one gametick.

private:
	bool newGame;	//True if you have lost and can start a new game, false otherwise.
	int gameCounter; //Counts how many cubes you have walked to.

	void onCubeTouch(unsigned int id); //Method to be run when the cube is touched, reacts acordingly to the state of the game.
};
