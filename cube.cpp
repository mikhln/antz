#include <sifteo.h>
#include "assets.gen.h"
#include "cube.h"

static AssetSlot MainSlot = AssetSlot::allocate()
    .bootstrap(GameAssets);


void Cube::init(CubeID cubeId) {

	this->id = cubeId;
	vid.initMode(BG0_SPR_BG1);		//Videomode for sprites
	this->vid.attach(id);
	vid.bg0.image(vec(0,0), Background);
	this->RandWalls();

}


void Cube::RandWalls(){
	Random r;
	int wallr;

	//Set all walls to false so the cube has no walls.
	wallTOP=false;
	wallBOTTOM=false;
	wallRIGHT=false;
	wallLEFT=false;

	//Hide the old walls
	this->vid.sprites[1].hide();
	this->vid.sprites[2].hide();
	this->vid.sprites[3].hide();
	this->vid.sprites[4].hide();

	// 1/4 chance that a wall apear on a given side.
	wallr=r.randint(0,3);
	if(wallr==0){
		wallTOP=true;
	}
	wallr=r.randint(0,3);
	if(wallr==0){
		wallLEFT=true;
	}
	wallr=r.randint(0,3);
	if(wallr==0){
		wallRIGHT=true;
	}
	wallr=r.randint(0,3);
	if(wallr==0){
		wallBOTTOM=true;
	}
	
	//Paint the new walls on the cube.
	if(this->wallTOP){this->vid.sprites[1].setImage(Horizontal,0);this->vid.sprites[1].move(0,0);}
	if(this->wallLEFT){this->vid.sprites[2].setImage(Vertical,0);this->vid.sprites[2].move(0,0);}
	if(this->wallRIGHT){this->vid.sprites[3].setImage(Vertical,0);this->vid.sprites[3].move(112,0);}
	if(this->wallBOTTOM){this->vid.sprites[4].setImage(Horizontal,0);this->vid.sprites[4].move(0,112);}
}
